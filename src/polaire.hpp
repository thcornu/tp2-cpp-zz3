#pragma once

#include "point.hpp"

class Cartesien;

class Polaire : public Point{
    double a;
    double d;

    public :
        Polaire(double a = 0, double d = 0);
        Polaire(Cartesien& c);
        double getDistance() const;
        void setDistance(double _d);
        double getAngle() const;
        void setAngle(double _a);
        void afficher(std::stringstream & flux) const;
        void convertir(Cartesien& c) const;
        void convertir(Polaire& p) const;

};
