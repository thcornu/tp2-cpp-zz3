#pragma once 

#include "point.hpp"

class Polaire;

class Cartesien : public Point {
    double x;
    double y;

    public :
        Cartesien(double _x = 0, double _y = 0);
        Cartesien(Polaire& p);
        double getX() const;
        void setX(double _x);
        double getY() const;
        void setY(double _y);
        void afficher(std::stringstream & flux) const;
        void convertir(Polaire& p) const;
        void convertir(Cartesien& c) const;

};
