#include "cartesien.hpp"
#include "polaire.hpp"
#include <math.h>

Cartesien::Cartesien(double _x, double _y) : x(_x), y(_y) {}
Cartesien::Cartesien(Polaire& p) : x(p.getDistance()*cos(p.getAngle()*(M_PI/180))), 
    y(p.getDistance()*sin(p.getAngle()*(M_PI/180))) {}


double Cartesien::getX() const {return x;}
void Cartesien::setX(double _x) {x = _x;}
double Cartesien::getY() const {return y;}
void Cartesien::setY(double _y) {y = _y;}
void Cartesien::afficher(std::stringstream & flux) const {
    flux << "(x=" << x << ";y=" << y << ")";
}
void Cartesien::convertir(Polaire& p) const {
    double theta = (x == 0) ? 0 : atan(y/x);

    p.setDistance(sqrt(x*x+y*y));
    p.setAngle(theta*(180/M_PI));
}
void Cartesien::convertir(Cartesien& c) const {
    c.setX(x);
    c.setY(y);
}

std::stringstream& operator<<(std::stringstream& ss, const Cartesien& c) {
    c.afficher(ss);
    return ss;
}