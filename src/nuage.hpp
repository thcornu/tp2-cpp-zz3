#pragma once

#include <vector>
#include <iostream>
#include "cartesien.hpp"
#include "polaire.hpp"

template <typename T>
class Nuage {
    std::vector<T> tab;

    public :
        void ajouter(T e) {tab.push_back(e);}
        unsigned int size() const {return tab.size();}

        using const_iterator = typename std::vector<T>::const_iterator;
        const_iterator begin() const {return tab.begin();}
        const_iterator end() const {return tab.end();}
};

template <typename T>
T barycentre_v1(Nuage<T>& n) {
    double x = 0, y = 0;

    for (auto i = n.begin(); i != n.end(); ++i) {
        Cartesien c;
        i->convertir(c);
        x += c.getX();
        y += c.getY();
    }

    if (n.size() > 0) {
        x /= n.size();
        y /= n.size();
    }

    return {x, y};
}

Polaire barycentre_v1(Nuage<Polaire>& n) {
    double x = 0, y = 0;

    for (auto i = n.begin(); i != n.end(); ++i) {
        Cartesien c;
        i->convertir(c);
        x += c.getX();
        y += c.getY();
    }

    if (n.size() > 0) {
        x /= n.size();
        y /= n.size();
    }

    Polaire p;
    Cartesien(x, y).convertir(p);

    return p;
}

template <typename T>
T barycentre_v2(Nuage<T>& n) {
    double x = 0, y = 0;

    for (auto i = n.begin(); i != n.end(); ++i) {
        Cartesien c;
        i->convertir(c);
        x += c.getX();
        y += c.getY();
    }

    if (n.size() > 0) {
        x /= n.size();
        y /= n.size();
    }

    return {x, y};
}

template <typename T>
T barycentre_v2(std::vector<T>& n) {
    double x = 0, y = 0;

    for (auto i = n.begin(); i != n.end(); ++i) {
        Cartesien c;
        i->convertir(c);
        x += c.getX();
        y += c.getY();
    }

    if (n.size() > 0) {
        x /= n.size();
        y /= n.size();
    }

    return {x, y};
}

Polaire barycentre_v2(Nuage<Polaire>& n) {
    double x = 0, y = 0;

    for (auto i = n.begin(); i != n.end(); ++i) {
        Cartesien c;
        i->convertir(c);
        x += c.getX();
        y += c.getY();
    }

    if (n.size() > 0) {
        x /= n.size();
        y /= n.size();
    }

    Polaire p;
    Cartesien(x, y).convertir(p);

    return p;
}

Polaire barycentre_v2(std::vector<Polaire>& n) {
    double x = 0, y = 0;

    for (auto i = n.begin(); i != n.end(); ++i) {
        Cartesien c;
        i->convertir(c);
        x += c.getX();
        y += c.getY();
    }

    if (n.size() > 0) {
        x /= n.size();
        y /= n.size();
    }

    Polaire p;
    Cartesien(x, y).convertir(p);

    return p;
}